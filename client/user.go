package client

import (
	"encoding/json"
	"errors"
)

// ErrInvalidUserIdentifier shall be returned when an invalid identifier is passed as argument.
// Examples include non-valid strings or empty string.
var ErrInvalidUserIdentifier = errors.New("invalid user identifier")

// User represents a member of a membership database.
type User struct {
	Identifier string
	Fields     map[string][]byte
}

// NewUser returns a new User with the corresponding identifier.
func NewUser(identifier string) *User {
	return &User{
		Identifier: identifier,
		Fields:     make(map[string][]byte),
	}
}

// MarshalText returns a JSON representation of one User.
func (u *User) MarshalText() (data []byte, err error) {
	return json.Marshal(u.Fields)
}

// UnmarshalText decodes JSON representing a User.
func (u *User) UnmarshalText(data []byte) error {
	return json.Unmarshal(data, &(u.Fields))
}
