package client

import (
	"context"
	"time"

	"gitlab.com/SporeDB/sporedb/db"
	"gitlab.com/SporeDB/sporedb/db/api"
	"gitlab.com/SporeDB/sporedb/db/client"
)

// Client is the structure used to connect to a membership node endpoint.
type Client struct {
	api    *client.Client
	policy string
	prefix string
}

// NewClient return a Client connected to the specified server.
func NewClient(server, policy, prefix string) (*Client, error) {
	c := &Client{
		api: &client.Client{
			Addr:    server,
			Timeout: time.Minute,
		},
		policy: policy,
		prefix: prefix,
	}

	return c, c.api.Connect()
}

// Add adds one user to a membership database.
func (c *Client) Add(u *User) error {
	if u.Identifier == "" {
		return ErrInvalidUserIdentifier
	}

	data, err := u.MarshalText()
	if err != nil {
		return err
	}

	tx := &api.Transaction{
		Operations: []*db.Operation{{
			Key:  c.prefix,
			Op:   db.Operation_SADD,
			Data: []byte(u.Identifier),
		}, {
			Key:  c.prefix + u.Identifier,
			Op:   db.Operation_SET,
			Data: data,
		}},
		Policy: c.policy,
	}

	ctx, done := getCtx()
	defer done()
	_, err = c.api.Submit(ctx, tx)
	return err
}

// Remove removes one user from a membership database.
func (c *Client) Remove(u *User) error {
	if u.Identifier == "" {
		return ErrInvalidUserIdentifier
	}

	tx := &api.Transaction{
		Operations: []*db.Operation{{
			Key:  c.prefix,
			Op:   db.Operation_SREM,
			Data: []byte(u.Identifier),
		}},
		Policy: c.policy,
	}

	ctx, done := getCtx()
	defer done()
	_, err := c.api.Submit(ctx, tx)
	return err
}

// Get returns a User if its contained in a membership database.
// Otherwise, it returns nil.
func (c *Client) Get(identifier string) (*User, error) {
	if identifier == "" {
		return nil, ErrInvalidUserIdentifier
	}

	u := NewUser(identifier)

	ctx, done := getCtx()
	defer done()

	contains, err := c.api.Contains(ctx, c.prefix, []byte(identifier))
	if err != nil {
		return nil, err
	}

	if !contains {
		return nil, nil
	}

	data, _, err := c.api.Get(ctx, c.prefix+identifier)
	if err != nil {
		return nil, err
	}

	return u, u.UnmarshalText(data)
}

// List returns the list of a membership database users.
func (c *Client) List() (members []string, err error) {
	ctx, done := getCtx()
	defer done()
	values, _, err := c.api.Members(ctx, c.prefix)
	for _, value := range values {
		members = append(members, string(value))
	}

	return
}

func getCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), time.Minute)
}
