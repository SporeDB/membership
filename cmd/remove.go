package cmd

import (
	"errors"

	"github.com/spf13/cobra"
	"gitlab.com/SporeDB/membership/client"
)

func init() {
	RootCmd.AddCommand(removeCmd)
}

var removeCmd = &cobra.Command{
	Use:   "remove identifier",
	Short: "Remove a member from the database",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			checkError(errors.New("remove command expects exactly one argument"))
		}

		c := getClient(cmd)
		checkError(c.Remove(client.NewUser(args[0])))
	},
}
