package cmd

import (
	"errors"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(isCmd)
}

var isCmd = &cobra.Command{
	Use:   "is identifier",
	Short: "Check if a member is already part of the membership",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			checkError(errors.New("is command expects exactly one argument"))
		}

		c := getClient(cmd)
		user, err := c.Get(args[0])
		checkError(err)

		if user == nil {
			fmt.Println(false)
			os.Exit(1)
		}

		fmt.Println(true)
	},
}
