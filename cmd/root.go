package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/SporeDB/membership/client"
)

// RootCmd is the Cobra root command for membership binary.
var RootCmd = &cobra.Command{
	Use:   "membership",
	Short: "A byzantine-tolerant decentralized membership service using SporeDB as its backend",
}

func init() {
	RootCmd.PersistentFlags().StringP("server", "s", "localhost:4200", "SporeDB node RPC API address")
	RootCmd.PersistentFlags().StringP("policy", "p", "membership", "SporeDB policy name")
	RootCmd.PersistentFlags().StringP("prefix", "x", "membership_", "Database membership prefix")
}

func getClient(cmd *cobra.Command) *client.Client {
	client, err := client.NewClient(
		cmd.Flag("server").Value.String(),
		cmd.Flag("policy").Value.String(),
		cmd.Flag("prefix").Value.String(),
	)
	checkError(err)
	return client
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}
