package cmd

import (
	"errors"

	"github.com/spf13/cobra"
	"gitlab.com/SporeDB/membership/client"
)

func init() {
	RootCmd.AddCommand(addCmd)
}

var addCmd = &cobra.Command{
	Use:   "add identifier",
	Short: "Add a member in the database",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			checkError(errors.New("add command expects exactly one argument"))
		}

		c := getClient(cmd)
		checkError(c.Add(client.NewUser(args[0])))
	},
}
