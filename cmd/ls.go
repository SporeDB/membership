package cmd

import (
	"fmt"
	"sort"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(lsCmd)
}

var lsCmd = &cobra.Command{
	Use:   "ls",
	Short: "List every member stored in the database",
	Run: func(cmd *cobra.Command, args []string) {
		c := getClient(cmd)
		members, err := c.List()
		checkError(err)

		sort.Sort(sort.StringSlice(members))
		for _, m := range members {
			fmt.Println(m)
		}
	},
}
