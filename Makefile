BASE_PATH=gitlab.com/SporeDB/membership

install:
	go get -t ./...

lint: install
	gometalinter -j 1 -t --deadline 1000s \
		--dupl-threshold 100 \
		--exclude ".pb.go" \
		--exclude "Errors unhandled." \
		./...
